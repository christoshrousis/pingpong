# Ping Pong

**Ping Pong** is a project forked by **Christos.Hrousis** from [Elovation](https://github.com/drewolson/elovation), a project created by [Drew Olsen]() of [Braintree](https://www.braintreepayments.com/braintrust/ping-pong-is-serious-business). 

**Ping Pong** is a rails app that tracks the results of any two player game and assigns ratings to the players using the [Elo rating system](http://en.wikipedia.org/wiki/Elo_rating_system).

The system also supports individual player rankings within multi-player teams, using the [Trueskill ranking system](http://research.microsoft.com/en-us/projects/trueskill/), but this functionality is ignored where possible.

#### Nounces
----

The app relies on Google+ OAuth, using the [OmniAuth gem](https://github.com/zquestz/omniauth-google-oauth2). The Google App and API keys are also administered **Christos.Hrousis'** accounts.

#### Deployment Instruction
----

Currently the app is hosted on **Christos.Hrousis'** heroku account, therefore is only deployable by request.