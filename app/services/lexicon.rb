module Lexicon
  class << self
    def defeated
      %w( defeated slayed defecated\ on educated pinged ponged
          blasted smashed obliterated stepped\ on slapped
          conquered won\ against triumphed\ over prevailed\ over
          vanquished frustrated annoyed best bashed ping\ ponged smoked
          put\ away ended demoralised pantsed finished ate\ up made\ a\ mockery\ of
          humiliated gave\ a\ clinic\ to handed\ it\ to posterised hall\ of\ shamed
          deminished smoked pulverised walloped f%^ked\ over f#@ked took\ out
          thwarted baulked rebuffed annulled nullified bested ruined embarrassed trumped obfuscated
          pantsed forestalled derailed decomissioned dismissed foiled debarred dethroned
          mauled pelted thwacked quashed trounced routed thrashed lambasted whipped ).sample
    end
  end
end