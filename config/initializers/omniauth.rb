GOOGLE_OAUTH_CONFIG = YAML.load_file("#{::Rails.root}/config/google_oauth.yml")[::Rails.env]

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, GOOGLE_OAUTH_CONFIG['GOOGLE_CLIENT_ID'], GOOGLE_OAUTH_CONFIG['GOOGLE_CLIENT_SECRET']
end